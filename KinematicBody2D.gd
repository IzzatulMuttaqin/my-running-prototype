extends KinematicBody2D

var ghost = self
onready var target = get_parent().get_node("Player")
var velocity = Vector2()
var speed = 2000

func _ready():
	set_physics_process(true)

func _process(delta):
	if target.position.x < position.x:
		velocity.x -= speed
	if target.position.y < position.y:
		velocity.y -= speed
	

func _physics_process(delta):
		# you get the position of the player
	var player_origin = target.get_global_transform().origin

	# you get the position of the enemy
	var enemy_origin = ghost.get_global_transform().origin
	
	# when you subtract enemy from player, what you get is kind of like the _direction_ towards the player, which we will store in offset
	var offset = player_origin - enemy_origin
	
	# normalize offset so that it is a Vector of length 1
	# multiply by our move speed and by delta
	#ren = move_and_collide(offset.normalized() * speed * delta)
	move_and_slide(offset.normalized() * speed * delta)
	#print(ren)

func _on_Ghost_area_entered(area):
	if area.is_in_group("player"):
		#print("success")
		get_tree().change_scene("res://Scenes/Game Over/Parent.tscn")
