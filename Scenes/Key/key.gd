extends Area2D

func _on_Key_area_entered(area):
	if area.is_in_group("player"):
		var node = get_node("../Player")
		node.getKey = true
		queue_free()
