extends Control

var enter_key = 16777221

func _input(event):
	if event is InputEventKey:
		#print(event.scancode)
		if Input.is_key_pressed(enter_key):
			get_tree().change_scene("res://Scenes/title_screen/TitleScreen.tscn")
